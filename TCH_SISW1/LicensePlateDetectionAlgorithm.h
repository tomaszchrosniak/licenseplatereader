#pragma once
#ifndef __INTERFACE_LICENSE_PLATE_DETECTION_ALGORITHM__
#define __INTERFACE_LICENSE_PLATE_DETECTION_ALGORITHM__

#include "opencv2/opencv.hpp"
#include <memory>

class LicensePlateDetectionAlgorithm
{
public:
	static std::unique_ptr<LicensePlateDetectionAlgorithm> licensePlateDetectionAlgorithm;

	virtual ~LicensePlateDetectionAlgorithm();

	virtual void setThresholdValue(int newThresholdValue) = 0;

	virtual cv::Mat getLicensePlateImageMatrix(cv::Mat inputImageMatrix) = 0;
};

#endif
