#include "LicensePlateLetterRecognitionAlgorithm.h"
#include <iostream>
#include <fstream>

using namespace std;

std::unique_ptr<LicensePlateLetterRecognitionAlgorithm> LicensePlateLetterRecognitionAlgorithm::licensePlateLetterRecognitionAlgorithm;

LicensePlateLetterRecognitionAlgorithm::~LicensePlateLetterRecognitionAlgorithm()
{

}

void LicensePlateLetterRecognitionAlgorithm::prepareTrainedData(void)
{
	ifstream schemaListFile;
	schemaListFile.open("Tomasz_Chrosniak//wzory_liter.txt", ifstream::in);
	while (!(schemaListFile.rdstate() & ifstream::eofbit))
	{
		this->schemaFilenames.push_back("");
		getline(schemaListFile, schemaFilenames[schemaFilenames.size() - 1]);
	}
}
