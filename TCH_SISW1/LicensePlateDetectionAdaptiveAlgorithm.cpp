#include "LicensePlateDetectionAdaptiveAlgorithm.h"

using namespace std;
using namespace cv;

LicensePlateDetectionAlgorithm* createLicensePlateDetectionAdaptiveAlgorithm()
{
	LicensePlateDetectionAlgorithm::licensePlateDetectionAlgorithm.reset(new LicensePlateDetectionAdaptiveAlgorithm());
	return LicensePlateDetectionAlgorithm::licensePlateDetectionAlgorithm.get();
}

LicensePlateDetectionAdaptiveAlgorithm::~LicensePlateDetectionAdaptiveAlgorithm()
{

}

void LicensePlateDetectionAdaptiveAlgorithm::setThresholdValue(int newThresholdValue)
{
	this->adaptiveThresholdWindowSize = newThresholdValue;
}

Mat LicensePlateDetectionAdaptiveAlgorithm::getLicensePlateImageMatrix(Mat inputImageMatrix)
{
	Mat scaledImage, image, outputImage, thresholdInputImage, histogramedImage, thresholdOutputImage, medianBluredImage, grayscaleImage, findContoursInputImage, dilatedImage, contrastedImage;

	resize(inputImageMatrix, scaledImage, Size(1280, inputImageMatrix.size().height * 1280 / inputImageMatrix.size().width));

	cvtColor(scaledImage, image, COLOR_BGR2GRAY);
	vector<Vec4i> hierarchy, filteredHierarchy;
	equalizeHist(image, histogramedImage);
	histogramedImage.convertTo(contrastedImage, -1, 2.5, -255.0);
	erode(contrastedImage, dilatedImage, Mat());
	thresholdInputImage = dilatedImage;
	adaptiveThreshold(thresholdInputImage, thresholdOutputImage, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, adaptiveThresholdWindowSize, secondParam);

	medianBlur(thresholdOutputImage, medianBluredImage, adaptiveThresholdMedianBlurSize);
	findContoursInputImage = medianBluredImage;
	vector<vector<Point>> contours, filteredContours;

	findContours(findContoursInputImage, filteredContours, filteredHierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

	vector<Moments> momentsVector;
	vector<Point> points;

	for (int i = 0; i < filteredContours.size(); ++i)
	{

		momentsVector.push_back(moments(filteredContours[i]));
		points.push_back(Point2f(momentsVector[i].m10 / momentsVector[i].m00, momentsVector[i].m01 / momentsVector[i].m00));
	}

	vector<vector<Point2i>> boundingBoxPoints;
	int minAreaContourId = 0;
	int currentMinimumArea = image.size().width * image.size().width * 2;
	int minimumArea = image.size().width * image.size().width / 54;
	double minimumLength = 1.0;
	double minimumWidth = 1.0;
	for (int i = 0; i < filteredContours.size(); ++i)
	{
		vector<Point2i> convexPoints;
		convexHull(filteredContours[i], convexPoints, false, true);
		boundingBoxPoints.push_back(convexPoints);

		RotatedRect rect = minAreaRect(convexPoints);
		Point2f boxPoints[4];
		rect.points(boxPoints);
		boundingBoxPoints[i] = (vector<Point2i>({ boxPoints[0], boxPoints[1], boxPoints[2], boxPoints[3] }));

#ifdef __DEBUG__
		unsigned long color = (i * (unsigned long)0xffffff / filteredContours.size());
		color = (unsigned long)0xffffff - color;
		drawContours(image, boundingBoxPoints, i, Scalar((color & 0xff0000) >> 16, (color & 0x00ff00) >> 8, (color & 0x0000ff)), 3);
#endif

		int tempMinimumArea = contourArea(filteredContours[i]);
		vector<double> sideLengths({arcLength(vector<Point2i>{boundingBoxPoints[i][0], boundingBoxPoints[i][1]}, false), arcLength(vector<Point2i>{boundingBoxPoints[i][1], boundingBoxPoints[i][2]}, false), arcLength(vector<Point2i>{boundingBoxPoints[i][2], boundingBoxPoints[i][3]}, false), arcLength(vector<Point2i>{boundingBoxPoints[i][3], boundingBoxPoints[i][0]}, false) });
		double length = *max_element(sideLengths.begin(), sideLengths.end());
		double width = *min_element(sideLengths.begin(), sideLengths.end());

		double condition3 = 1 + (sqrt(pow(((image.size().width / 2) - points[i].x), 2) + pow(((image.size().height / 2) - points[i].y), 2)) / (image.size().height / 4));
		double scaledTempMinimumArea = tempMinimumArea * condition3;
		if ((tempMinimumArea > minimumArea) && (scaledTempMinimumArea < currentMinimumArea) && ((length / width) < 7) && ((length / width) >= 3.2))
		{
			int numberOfInternalPoints = 0;
			if (pointPolygonTest(boundingBoxPoints[i], boundingBoxPoints[minAreaContourId][0], false) >= 0)numberOfInternalPoints++;
			if (pointPolygonTest(boundingBoxPoints[i], boundingBoxPoints[minAreaContourId][1], false) >= 0)numberOfInternalPoints++;
			if (pointPolygonTest(boundingBoxPoints[i], boundingBoxPoints[minAreaContourId][2], false) >= 0)numberOfInternalPoints++;
			if (pointPolygonTest(boundingBoxPoints[i], boundingBoxPoints[minAreaContourId][3], false) >= 0)numberOfInternalPoints++;
			if (numberOfInternalPoints >= 2)
				continue;
			minAreaContourId = i;
			minimumLength = length;
			minimumWidth = width;
			currentMinimumArea = scaledTempMinimumArea;
		}
	}
	if (minimumLength == 1.0 && minimumWidth == 1.0)
		return Mat();
	Point2f dstPoints[4];
	if (arcLength(vector<Point>({ boundingBoxPoints[minAreaContourId][0], boundingBoxPoints[minAreaContourId][1] }), false) < arcLength(vector<Point>({ boundingBoxPoints[minAreaContourId][1], boundingBoxPoints[minAreaContourId][2] }), false))
	{
		dstPoints[0] = Point2f(0, minimumWidth);
		dstPoints[1] = Point2f(0, 0);
		dstPoints[2] = Point2f(minimumLength, 0);
		dstPoints[3] = Point2f(minimumLength, minimumWidth);
	}
	else
	{
		dstPoints[1] = Point2f(0, minimumWidth);
		dstPoints[2] = Point2f(0, 0);
		dstPoints[3] = Point2f(minimumLength, 0);
		dstPoints[0] = Point2f(minimumLength, minimumWidth);
	}

	Point2f srcpoints[4] = { boundingBoxPoints[minAreaContourId][0], boundingBoxPoints[minAreaContourId][1], boundingBoxPoints[minAreaContourId][2], boundingBoxPoints[minAreaContourId][3] };

	Mat licensePlateImage = Mat::zeros(Size(minimumLength, minimumWidth), image.type()), transformMatrix = getPerspectiveTransform(srcpoints, dstPoints);
	warpPerspective(image, licensePlateImage, transformMatrix, Size(minimumLength, minimumWidth));
#ifdef __DEBUG__
	drawContours(image, boundingBoxPoints, minAreaContourId, Scalar(0, 0, 255), 3);
	namedWindow("picture");
	namedWindow("thresholdedImage");
	namedWindow("licensePlate");
	createTrackbar("thresholdTrackbar", "thresholdedImage", &thresholdVal, 255);
	createTrackbar("adaptiveThresholdTrackbar", "thresholdedImage", &adaptiveThresholdWindowSize, 255);
	createTrackbar("2ndParamAdaptiveThresholdTrackbar", "thresholdedImage", &secondParam, 30);
	createTrackbar("adaptiveThresholdMedianBlurSize", "thresholdedImage", &adaptiveThresholdMedianBlurSize, 255);

	int key;
	while ((key = waitKey(50)) <= 0)
	{
		imshow("picture", image);
		if (adaptiveThresholdWindowSize < 3)
			adaptiveThresholdWindowSize = 3;
		if (adaptiveThresholdWindowSize % 2 == 0)
			adaptiveThresholdWindowSize++;

		if (adaptiveThresholdMedianBlurSize < 3)
			adaptiveThresholdMedianBlurSize = 3;
		if (adaptiveThresholdMedianBlurSize % 2 == 0)
			adaptiveThresholdMedianBlurSize++;

		if (secondParam == 0)
			secondParam++;

		adaptiveThreshold(thresholdInputImage, thresholdOutputImage, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, adaptiveThresholdWindowSize, secondParam);
		medianBlur(thresholdOutputImage, medianBluredImage, adaptiveThresholdMedianBlurSize);
		imshow("thresholdedImage", medianBluredImage);
		imshow("licensePlate", licensePlateImage);
	}
#endif
	return licensePlateImage;
}
