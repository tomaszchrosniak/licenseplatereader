#pragma once
#ifndef __IMPLEMENTATION_LICENSE_PLATE_LETTER_RECOGNITION_FEATURE_MATCHER_ALGORITHM__
#define __IMPLEMENTATION_LICENSE_PLATE_LETTER_RECOGNITION_FEATURE_MATCHER_ALGORITHM__

#include "LicensePlateLetterRecognitionAlgorithm.h"
#include <math.h>

using namespace std;

LicensePlateLetterRecognitionAlgorithm* createLicensePlateLetterRecognitionFeatureMatcherAlgorithm();

class LicensePlateLetterRecognitionFeatureMatcherAlgorithm: virtual public LicensePlateLetterRecognitionAlgorithm
{
private:
	int patchSize = 25;
public:
	LicensePlateLetterRecognitionFeatureMatcherAlgorithm();

	virtual ~LicensePlateLetterRecognitionFeatureMatcherAlgorithm();

	virtual std::vector<char> getLicensePlateLetters(cv::Mat inputImageMatrix);
};

#endif
