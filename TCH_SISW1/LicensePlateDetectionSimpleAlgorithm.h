#pragma once
#ifndef __IMPLEMENTATION_LICENSE_PLATE_DETECTION_ALGORITHM_SIMPLE__
#define __IMPLEMENTATION_LICENSE_PLATE_DETECTION_ALGORITHM_SIMPLE__

#include "LicensePlateDetectionAlgorithm.h"
#include <vector>

using namespace std;

LicensePlateDetectionAlgorithm* createLicensePlateDetectionSimpleAlgorithm();

class LicensePlateDetectionSimpleAlgorithm : virtual public LicensePlateDetectionAlgorithm
{
private:
	int secondParam = 3;
	int thresholdVal = 242;
	int adaptiveThresholdWindowSize = 21;
	int adaptiveThresholdMedianBlurSize = 11;
public:
	virtual ~LicensePlateDetectionSimpleAlgorithm();

	virtual void setThresholdValue(int newThresholdValue);

	virtual cv::Mat getLicensePlateImageMatrix(cv::Mat inputImageMatrix);
};

#endif
