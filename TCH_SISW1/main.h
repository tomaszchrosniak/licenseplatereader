#ifndef __MAIN_H__
#define __MAIN_H_
#include "opencv2/opencv.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <omp.h>
#include "LicensePlateDetectionAdaptiveAlgorithm.h"
#include "LicensePlateDetectionSimpleAlgorithm.h"
#include "LicensePlateLetterRecognitionFeatureMatcherAlgorithm.h"
#include "LicensePlateLetterRecognitionStructuralAnalysisAlgorithm.h"
#ifdef __DEBUG__
#include <time.h>
#endif

#endif
