#pragma once
#ifndef __IMPLEMENTATION_LICENSE_PLATE_DETECTION_ALGORITHM_ADAPTIVE__
#define __IMPLEMENTATION_LICENSE_PLATE_DETECTION_ALGORITHM_ADAPTIVE__

#include "LicensePlateDetectionAlgorithm.h"
#include <vector>

using namespace std;

LicensePlateDetectionAlgorithm* createLicensePlateDetectionAdaptiveAlgorithm();

class LicensePlateDetectionAdaptiveAlgorithm : virtual public LicensePlateDetectionAlgorithm
{
private:
	int secondParam = 3;
	int thresholdVal = 242;
	int adaptiveThresholdWindowSize = 21;
	int adaptiveThresholdMedianBlurSize = 11;
public:
	virtual ~LicensePlateDetectionAdaptiveAlgorithm();

	virtual void setThresholdValue(int newThresholdValue);

	virtual cv::Mat getLicensePlateImageMatrix(cv::Mat inputImageMatrix);
};

#endif
