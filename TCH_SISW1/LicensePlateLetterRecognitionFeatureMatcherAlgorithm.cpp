#include "LicensePlateLetterRecognitionFeatureMatcherAlgorithm.h"

using namespace std;
using namespace cv;

LicensePlateLetterRecognitionAlgorithm* createLicensePlateLetterRecognitionFeatureMatcherAlgorithm()
{
	LicensePlateLetterRecognitionAlgorithm::licensePlateLetterRecognitionAlgorithm.reset(new LicensePlateLetterRecognitionFeatureMatcherAlgorithm());
	return LicensePlateLetterRecognitionAlgorithm::licensePlateLetterRecognitionAlgorithm.get();
}

LicensePlateLetterRecognitionFeatureMatcherAlgorithm::LicensePlateLetterRecognitionFeatureMatcherAlgorithm()
{
	this->prepareTrainedData();
}

LicensePlateLetterRecognitionFeatureMatcherAlgorithm::~LicensePlateLetterRecognitionFeatureMatcherAlgorithm()
{

}

vector<char> LicensePlateLetterRecognitionFeatureMatcherAlgorithm::getLicensePlateLetters(Mat inputImageMatrix)
{
	vector<char> znaki;
	vector<int> xcoord;

	for (int l = 0; l < 7; ++l)
	{
		string currentFileSchema;
		Mat thresholdOutputImage, thresholdInputImage, medianBluredImage, sideBySideImage, letter;
		int plateThresholdVal = 120;
		erode(inputImageMatrix, thresholdInputImage, Mat());
		threshold(thresholdInputImage, thresholdOutputImage, plateThresholdVal, 255, THRESH_BINARY);
		medianBlur(thresholdOutputImage, medianBluredImage, 3);

		Mat letterU, outputImage;;
		vector<vector<Point>> contours;

		vector<double> contourAreas(7, 0.0);
		vector<Vec4i> hierarchy;
		vector<vector<Point>> foundLetters(7, vector<Point>({ Point(inputImageMatrix.size().width, 0), Point(inputImageMatrix.size().width, 1), Point(inputImageMatrix.size().width - 1, 1), Point(inputImageMatrix.size().width - 1, 0) }));
		findContours(medianBluredImage, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
		drawContours(medianBluredImage, contours, -1, Scalar(255, 0, 0));
		namedWindow("medianBluredImage");
		while (waitKey(50) <= 0)
			imshow("medianBluredImage", medianBluredImage);
		for (int i = 0; i < contours.size(); ++i)
		{
			int numberOfChildren = 0;
			for (int j = 0; j < hierarchy.size(); ++j)
			{
				if (hierarchy[j][3] == i)
					numberOfChildren++;
				if (numberOfChildren >= 7)
					break;
			}
			if (numberOfChildren >= 7)
			{
				for (int j = 0; j < hierarchy.size(); ++j)
				{
					if (hierarchy[j][3] == i)
					{
						if (contourArea(contours[j])>*min_element(contourAreas.begin(), contourAreas.end()))
						{
							contourAreas.erase(contourAreas.begin());
							contourAreas.push_back(contourArea(contours[j]));
							foundLetters.erase(foundLetters.begin());
							foundLetters.push_back(contours[j]);
						}
					}
				}
				break;
			}
		}
		bool changes = false;
		do
		{
			changes = false;
			for (int i = 0; i < foundLetters.size() - 1;)
			{
				if (foundLetters[i][0].x>foundLetters[i + 1][0].x)
				{
					foundLetters.push_back(foundLetters[i]);
					foundLetters.erase(foundLetters.begin() + i);
					changes = true;
				}
				else
					i++;
			}
		} while (changes);

		vector<Mat> letters(7);
		for (int i = 0; i < foundLetters.size(); ++i)
		{
			Rect rect = boundingRect(foundLetters[i]);
			int x = rect.x, y = rect.y, localWidth = rect.width, localHeight = rect.height;
			vector<int> xcoords({ x, x + localWidth-1 });
			vector<int> ycoords({ y, y + localHeight-1 });
			Point2f srcpoints[4], dstPoints[4];
			srcpoints[0] = Point2f(0, 0);
			srcpoints[1] = Point2f(0, 0 + localHeight);
			srcpoints[2] = Point2f(0 + localWidth, 0 + localHeight);
			srcpoints[3] = Point2f(0 + localWidth, 0);

			dstPoints[0] = Point2f(0, 0);
			dstPoints[1] = Point2f(0, 228);
			dstPoints[2] = Point2f(176, 228);
			dstPoints[3] = Point2f(176, 0);
			Mat transformMatrix = getPerspectiveTransform(srcpoints, dstPoints);
			Mat letterFromPlate = inputImageMatrix.colRange(Range(*min_element(xcoords.begin(), xcoords.end()), *max_element(xcoords.begin(), xcoords.end()))).rowRange(Range(*min_element(ycoords.begin(), ycoords.end()), *max_element(ycoords.begin(), ycoords.end()))).clone();
			warpPerspective(letterFromPlate, letters[i], transformMatrix, Size(176, 228));

#ifdef __DEBUG__
			{
				namedWindow("outputImage");
				vector<vector<Point>> contours2;
				vector<Vec4i> hierarchy2;
				Mat letterContour, thresholdedLetter;
				int thresholdValue = 140;
				createTrackbar("Letter threshold value", "outputImage", &thresholdValue, 255);
				while (waitKey(50) <= 0)
				{
					outputImage = letters[i].clone();
					letterContour = Mat::zeros(outputImage.rows, outputImage.cols, outputImage.type());
					threshold(outputImage, thresholdedLetter, thresholdValue, 255, THRESH_BINARY);
					findContours(thresholdedLetter, contours2, hierarchy2, RETR_TREE, CHAIN_APPROX_SIMPLE);
					if (contours2.size()>1)
						drawContours(letterContour, contours2, 1, Scalar(255, 255, 255), 3);
					imshow("outputImage", letterContour);
				}
			}
#endif
			for (int j = 0; j < schemaFilenames.size(); ++j)
			{
				currentFileSchema = schemaFilenames[j];
				letter = imread(currentFileSchema, IMREAD_GRAYSCALE);
				Ptr<ORB> featureDetector = ORB::create();
				Ptr<DescriptorMatcher> bruteForceMatcher = DescriptorMatcher::create("BruteForce-Hamming");

				vector<KeyPoint> keypointsQuery, keypointsTrain;

				featureDetector->setNLevels(10);
				featureDetector->setEdgeThreshold(0);
				featureDetector->setPatchSize(patchSize);

				featureDetector->detect(letter, keypointsQuery);
				featureDetector->detect(letters[i], keypointsTrain);

				Mat descriptorsQuery, descriptorsTrain;

				featureDetector->compute(letter, keypointsQuery, descriptorsQuery);
				featureDetector->compute(letters[i], keypointsTrain, descriptorsTrain);

#ifdef __USE_BRUTE_FORCE__
				vector<vector<DMatch>> matches;
#endif
#ifndef __USE_BRUTE_FORCE__
				vector<vector<DMatch>> matches;
#endif
				vector<DMatch> filteredMatches, homographyMatches;

				double minDist = 100, maxDist = 0;

				bruteForceMatcher->knnMatch(descriptorsQuery, descriptorsTrain, matches, 2);

				for (int z = 0; z < matches.size(); ++z)
				{
					for (int m = 0; m < matches[l].size(); ++m)
					{
						if (matches[z][m].distance>maxDist) maxDist = matches[z][m].distance;
						if (matches[z][m].distance < minDist) minDist = matches[z][m].distance;
					}
				}

				vector<Point2f> srcPts, dstPts;
				for (int z = 0; z < matches.size(); ++z)
				{
					if ((float)matches[z][0].distance <= 0.8*(float)matches[z][1].distance && (float)matches[z][0].distance<minDist*3)
					{
						srcPts.push_back(keypointsQuery[matches[z][0].queryIdx].pt);
						dstPts.push_back(keypointsTrain[matches[z][0].trainIdx].pt);
					}
				}
				if (srcPts.size() ==0)
					continue;

				Mat mask;
				Mat homographyMatrix = findHomography(srcPts, dstPts);
				
				if (homographyMatrix.empty())
					continue;

				Mat tmp_corners, scene_corners;
				tmp_corners.create(4, 1, CV_32FC2);
				tmp_corners.at<Point2f>(Point2i(0, 0)) = Point2f(0.0, 0.0);
				tmp_corners.at<Point2f>(Point2i(0, 1)) = Point2f(inputImageMatrix.size().width, 0.0);
				tmp_corners.at<Point2f>(Point2i(0, 2)) = Point2f(inputImageMatrix.size().width, inputImageMatrix.size().height);
				tmp_corners.at<Point2f>(Point2i(0, 3)) = Point2f(0, inputImageMatrix.size().height);
				scene_corners.create(4, 1, CV_32FC2);

				drawMatches(letter, keypointsQuery, inputImageMatrix, keypointsTrain, filteredMatches, outputImage);

				for (int z = 0; z < homographyMatches.size(); ++z)
				{
					znaki.push_back(currentFileSchema.at(0));
					xcoord.push_back(keypointsTrain[homographyMatches[z].trainIdx].pt.x);
				}
			}
		}

		

	}
	vector<char> orderedChars;
	vector<int>::iterator xcoordIter;
	for (int y = 0; y < znaki.size(); ++y)
	{
		orderedChars.push_back(*(znaki.begin() + (xcoord.begin() - (xcoordIter = min_element(xcoord.begin(), xcoord.end())))));
		znaki.erase(znaki.begin() + (xcoord.begin() - xcoordIter));
		xcoord.erase(xcoordIter);
	}
	return orderedChars;
}
