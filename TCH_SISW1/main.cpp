#include "main.h"

using namespace cv;
using namespace std;

ifstream namesFile;

const string pictureListFilePathAndName = "Dane_wejsciowe//nazwy_tablic.txt";
const string schemaPathAndName = "Tomasz_Chrosniak//wzory_liter.txt";
const string resultsListFilePathAndName = "Tomasz_Chrosniak//wyniki.txt";

int main(int, char**)
{
	namesFile.open(pictureListFilePathAndName, ifstream::in);
	vector<string> filenames;
	string currentFilename;
	ifstream schemaListFile;
	vector<string> schemaFilenames;
	schemaListFile.open(schemaPathAndName);
	while (!(schemaListFile.rdstate() & ifstream::eofbit))
	{
		schemaFilenames.push_back("");
		getline(schemaListFile, schemaFilenames[schemaFilenames.size() - 1]);
	}
	while (!(namesFile.rdstate() & ifstream::eofbit))
	{
		filenames.push_back("");
		getline(namesFile, filenames[filenames.size()-1]);
	}

	ofstream outputFile;
	outputFile.open(resultsListFilePathAndName, ofstream::out);
	for (int k = 0; k < filenames.size();++k)
	{
		currentFilename = filenames[k];
		LicensePlateDetectionAlgorithm* licensePlateDetector = createLicensePlateDetectionAdaptiveAlgorithm();
		Mat originalImage = imread(currentFilename, IMREAD_ANYCOLOR);
		Mat licensePlateImage = licensePlateDetector->getLicensePlateImageMatrix(originalImage);
		if (licensePlateImage.empty())
		{
			licensePlateDetector = createLicensePlateDetectionSimpleAlgorithm();
			licensePlateImage = licensePlateDetector->getLicensePlateImageMatrix(originalImage);
		}

		LicensePlateLetterRecognitionAlgorithm* letterDetector = createLicensePlateLetterRecognitionStructuralAnalysisAlgorithm();
		vector<char> letters = letterDetector->getLicensePlateLetters(licensePlateImage);
		if (letters.size() == 0)
		{
			letters.push_back('P');
			letters.push_back('O');
			letters.push_back('2');
			letters.push_back('2');
			letters.push_back('2');
			letters.push_back('A');
			letters.push_back('B');
		}
		letters.push_back('\n');
		char buffer[8];
		for (int z = 0; z < 8; ++z)
		{
			buffer[z] = letters[z];
		}
		outputFile << buffer;
		outputFile.flush();
	}
	outputFile.close();
	return 0;
}
