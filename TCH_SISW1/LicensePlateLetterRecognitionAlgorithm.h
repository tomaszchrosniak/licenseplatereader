#pragma once
#ifndef __INTERFACE_LICENSE_PLATE_LETTER_RECOGNITION_ALGORITHM__
#define __INTERFACE_LICENSE_PLATE_LETTER_RECOGNITION_ALGORITHM__

#include "opencv2/opencv.hpp"
#include <memory>
#include <vector>

class LicensePlateLetterRecognitionAlgorithm
{
protected:
	std::vector<std::string> schemaFilenames;
public:
	static std::unique_ptr<LicensePlateLetterRecognitionAlgorithm> licensePlateLetterRecognitionAlgorithm;

	virtual ~LicensePlateLetterRecognitionAlgorithm();

	virtual void prepareTrainedData(void);

	virtual std::vector<char> getLicensePlateLetters(cv::Mat inputImageMatrix) = 0;
};

#endif
