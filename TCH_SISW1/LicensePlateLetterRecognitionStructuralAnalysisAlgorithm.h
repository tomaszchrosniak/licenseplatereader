#pragma once
#ifndef __IMPLEMENTATION_LICENSE_PLATE_LETTER_RECOGNITION_STRUCTURAL_ANALYSIS_ALGORITHM__
#define __IMPLEMENTATION_LICENSE_PLATE_LETTER_RECOGNITION_STRUCTURAL_ANALYSIS_ALGORITHM__

#include "LicensePlateLetterRecognitionAlgorithm.h"

using namespace std;

LicensePlateLetterRecognitionAlgorithm* createLicensePlateLetterRecognitionStructuralAnalysisAlgorithm();

class LicensePlateLetterRecognitionStructuralAnalysisAlgorithm : virtual public LicensePlateLetterRecognitionAlgorithm
{
private:
	static std::vector<char> characters;

	int patchSize = 25;
public:
	LicensePlateLetterRecognitionStructuralAnalysisAlgorithm();

	virtual ~LicensePlateLetterRecognitionStructuralAnalysisAlgorithm();

	virtual std::vector<char> getLicensePlateLetters(cv::Mat inputImageMatrix);
};

#endif
