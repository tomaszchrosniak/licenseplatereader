#include "LicensePlateLetterRecognitionStructuralAnalysisAlgorithm.h"

using namespace std;
using namespace cv;

vector<char> LicensePlateLetterRecognitionStructuralAnalysisAlgorithm::characters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'W', 'V', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

LicensePlateLetterRecognitionAlgorithm* createLicensePlateLetterRecognitionStructuralAnalysisAlgorithm()
{
	LicensePlateLetterRecognitionAlgorithm::licensePlateLetterRecognitionAlgorithm.reset(new LicensePlateLetterRecognitionStructuralAnalysisAlgorithm());
	return LicensePlateLetterRecognitionAlgorithm::licensePlateLetterRecognitionAlgorithm.get();
}

LicensePlateLetterRecognitionStructuralAnalysisAlgorithm::LicensePlateLetterRecognitionStructuralAnalysisAlgorithm()
{
	this->prepareTrainedData();
}

LicensePlateLetterRecognitionStructuralAnalysisAlgorithm::~LicensePlateLetterRecognitionStructuralAnalysisAlgorithm()
{

}

vector<char> LicensePlateLetterRecognitionStructuralAnalysisAlgorithm::getLicensePlateLetters(Mat inputImageMatrix)
{
	vector<char> chars;
	string currentFileSchema;
	Mat thresholdOutputImage, thresholdInputImage, medianBluredImage, sideBySideImage;
	int plateThresholdVal = 121;
	erode(inputImageMatrix, thresholdInputImage, Mat());
#ifdef __DEBUG__
	namedWindow("medianBluredImage");
	createTrackbar("plateThresholdValue", "medianBluredImage", &plateThresholdVal, 255);
	while (waitKey(50) <= 0)
	{
#endif
		threshold(thresholdInputImage, thresholdOutputImage, plateThresholdVal, 255, THRESH_BINARY);
#ifdef __DEBUG__
		imshow("medianBluredImage", thresholdOutputImage);
	}
#endif

	vector<vector<Point>> contours;
	vector<double> contourAreas(7, 0.0);
	vector<Vec4i> hierarchy;
	vector<vector<Point>> foundLetters(7, vector<Point>({ Point(inputImageMatrix.size().width, 0), Point(inputImageMatrix.size().width, 1), Point(inputImageMatrix.size().width - 1, 1), Point(inputImageMatrix.size().width - 1, 0) }));

	findContours(thresholdOutputImage, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
	bool found = false;
	for (int i = 0; i < contours.size(); ++i)
	{
		int numberOfChildren = 0;
		for (int j = 0; j < hierarchy.size(); ++j)
		{
			if (hierarchy[j][3] == i)
				numberOfChildren++;
			if (numberOfChildren >= 7)
				break;
		}
		if (numberOfChildren >= 7)
		{
			for (int j = 0; j < hierarchy.size(); ++j)
			{
				if (hierarchy[j][3] == i)
				{
					if (contourArea(contours[j])>*min_element(contourAreas.begin(), contourAreas.end()))
					{
						contourAreas.erase(contourAreas.begin());
						contourAreas.push_back(contourArea(contours[j]));
						foundLetters.erase(foundLetters.begin());
						foundLetters.push_back(contours[j]);
					}
				}
			}
			if (contourAreas[0] < 40.0)
			{
				found = false;
				continue;
			}
			else
			{
				found = true;
				break;
			}
		}
	}
	if (!found)
		return vector<char>();
	bool changes = false;
	do
	{
		changes = false;
		for (int i = 0; i < foundLetters.size() - 1;)
		{
			if (foundLetters[i][0].x>foundLetters[i + 1][0].x)
			{
				foundLetters.push_back(foundLetters[i]);
				foundLetters.erase(foundLetters.begin() + i);
				changes = true;
			}
			else
				i++;
		}
	} while (changes);

	vector<Point> convexPoints;
	vector<vector<Point2i>> boundingBoxPointsForLetter;
	vector<Mat> letters(7);
	for (int i = 0; i < foundLetters.size(); ++i)
	{
		Rect rect = boundingRect(foundLetters[i]);
		int x = rect.x, y = rect.y, localWidth = rect.width, localHeight = rect.height;
		vector<int> xcoords({ x, x + localWidth-1 });
		vector<int> ycoords({ y, y + localHeight-1 });
		Point2f srcpoints[4], dstPoints[4];
		srcpoints[0] = Point2f(0, 0);
		srcpoints[1] = Point2f(0, 0 + localHeight);
		srcpoints[2] = Point2f(0 + localWidth, 0 + localHeight);
		srcpoints[3] = Point2f(0 + localWidth, 0);

		Mat letterFromPlate = inputImageMatrix.colRange(Range(*min_element(xcoords.begin(), xcoords.end()), *max_element(xcoords.begin(), xcoords.end()))).rowRange(Range(*min_element(ycoords.begin(), ycoords.end()), *max_element(ycoords.begin(), ycoords.end()))).clone();


		Mat I1, I2;
		int d = CV_32F;
		const double C1 = 6.5025, C2 = 58.5225;
		int characterNumber = 0;
		double maxSimilarity = 0;

		for (int iterator = 0; iterator < schemaFilenames.size(); ++iterator)
		{
				
			currentFileSchema = schemaFilenames[iterator];
			Mat letterU = imread(currentFileSchema, IMREAD_GRAYSCALE), outputImage;

			dstPoints[0] = Point2f(0, 0);
			dstPoints[1] = Point2f(0, letterU.size().height);
			dstPoints[2] = Point2f(letterU.size().width,letterU.size().height);
			dstPoints[3] = Point2f(letterU.size().width, 0);
			Mat transformMatrix = getPerspectiveTransform(srcpoints, dstPoints);
			warpPerspective(letterFromPlate, letters[i], transformMatrix, Size(letterU.size().width,letterU.size().height));

			letters[i].convertTo(I1, d);
			Mat I1_2 = I1.mul(I1);

			letterU.convertTo(I2, d);
			Mat I2_2 = I2.mul(I2);
			Mat I1_I2 = I1.mul(I2);

			Mat mu1, mu2;
			GaussianBlur(I1, mu1, Size(11, 11), 1.5);
			GaussianBlur(I2, mu2, Size(11, 11), 1.5);

			Mat mu1_2 = mu1.mul(mu1);
			Mat mu2_2 = mu2.mul(mu2);
			Mat mu1_mu2 = mu1.mul(mu2);

			Mat sigma1_2, sigma2_2, sigma12;

			GaussianBlur(I1_2, sigma1_2, Size(11, 11), 1.5);
			sigma1_2 -= mu1_2;

			GaussianBlur(I2_2, sigma2_2, Size(11, 11), 1.5);
			sigma2_2 -= mu2_2;

			GaussianBlur(I1_I2, sigma12, Size(11, 11), 1.5);
			sigma12 -= mu1_mu2;

			///////////////////////////////// FORMULA ////////////////////////////////
			Mat t1, t2, t3;

			t1 = 2 * mu1_mu2 + C1;
			t2 = 2 * sigma12 + C2;
			t3 = t1.mul(t2);              // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))

			t1 = mu1_2 + mu2_2 + C1;
			t2 = sigma1_2 + sigma2_2 + C2;
			t1 = t1.mul(t2);               // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))

			Mat ssim_map;
			divide(t3, t1, ssim_map);      // ssim_map =  t3./t1;

			Scalar mssim = mean(ssim_map); // mssim = average of ssim map
			Mat s1;
			absdiff(I1, I2, s1);       // |I1 - I2|
			s1.convertTo(s1, CV_32F);  // cannot make a square on 8 bits
			s1 = s1.mul(s1);           // |I1 - I2|^2

			Scalar s = sum(s1);         // sum elements per channel

			double sse = s.val[0] + s.val[1] + s.val[2]; // sum channels
			double psnr;
			if (sse <= 1e-10) // for small values return zero
				psnr = 0;
			else
			{
				double  mse = sse / (double)(I1.channels() * I1.total());
				psnr = 10.0*log10((255 * 255) / mse);
			}
			if (psnr > maxSimilarity)
			{
				maxSimilarity = psnr;
				characterNumber = iterator;
			}
		}
#ifdef __DEBUG__
		namedWindow("outputImage");
		Mat outputImage = letters[i].clone();
		while (waitKey(50) <= 0)
			imshow("outputImage", outputImage);
#endif
		chars.push_back(characters[characterNumber%characters.size()]);
	}
	return chars;
}
